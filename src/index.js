import {utils} from './utils';
import {Router} from './router.js';
import HomeControler from './controllers/homeCtrl';
import ContactControler from './controllers/contactCtrl';
import CookiesController from './controllers/cookiesCtrl';
import AvisoLegalController from './controllers/avisoLegalCtrl';
import TarifasController from './controllers/tarifasCtrl';
import CatalegController from './controllers/catalogoCtrl';
import {footerHTML} from './templates/footer';
import {menuHTML} from './templates/menu';
import {cookiesHTML} from './templates/cookiesFloat';

utils.get('datosHome',function(response){
    localStorage.setItem('datosHome', response);
});

utils.get('datosEmpresa', function(response){
    /* Se guardan los datos de empresa */
    localStorage.setItem('datosEmpresa', response);
});

utils.get('tarifas', function(response){
    /* Se guardan los datos de las tarifas */
    localStorage.setItem('tarifas', response);
});

utils.get('articulos', function(response){
    /* Se guardan los datos de los articulos */
    localStorage.setItem('articulos', response);
});

document.addEventListener("DOMContentLoaded",async function(){

    /* Se recogen los datos solicitados del local storage mediante promesa
        (si no existe en el local storage se recogeran del servidor) */
    Router.add(/contact/, function() {
        console.log(utils.getCookie("idioma"));
        new ContactControler();
    }).listen()
    Router.add(/cookies/,async function() {
        console.log(utils.getCookie("idioma"));
        new CookiesController(await utils.getLocal('datosEmpresa'));
    }).listen()
    Router.add(/avisolegal/,async function() {
        console.log(utils.getCookie("idioma"));
        new AvisoLegalController(await utils.getLocal('datosEmpresa'));
    }).listen()
    Router.add(/tarifas/,async function() {
        console.log(utils.getCookie("idioma"));
        new TarifasController(await utils.getLocal('allTarifas'));
    }).listen()
    Router.add(/cataleg/,async function() {
        console.log("Cataleg")
        console.log(utils.getCookie("idioma"));
        new CatalegController(await utils.getLocal('articulos'));
    }).listen()
    Router.add(async function()  {
        console.log(utils.getCookie("idioma"));
        new HomeControler( await utils.getLocal('tarifas'), await utils.getLocal('datosEmpresa'), await utils.getLocal('datosHome'));
    }).listen();

    /* Como principal navega a home */
    Router.navigate("/home");

    /* Pintar menu y footer */
    document.getElementById("menu").innerHTML = menuHTML(await utils.getLocal('datosEmpresa'));
    document.getElementById("footer").innerHTML = footerHTML(await utils.getLocal('datosEmpresa'));

    /* Recoge de local storage si ha aceptado las cookies, si no pinta la advertencia */
    let c = localStorage.getItem('cookies');
    if (!c){
        document.getElementById("cookies").innerHTML = cookiesHTML();
    
        /* Este evento esta pendiente de si el usuario acepta la politica de cookies,
        una vez este las acepte se guarda en local storage para que no se vuelva a mostrar */
        document.getElementById("borrarCookies").addEventListener("click",
        function(){
            localStorage.setItem('cookies', true);
            document.getElementById("cookies").style.display = "none";
        });
    }

    /* LISTENERS */

        /* Listener menu*/
        document.getElementById('menuTable__homeBtn').addEventListener('click', 
        function(){
            /* Cuando se aprieta el botón navegamos a home */
            Router.navigate("/home");
        });
        
        /* Listener del boton de contact */
        document.getElementById("menuTable__contactBtn").addEventListener("click",
        function(){
            /* Cuando se aprieta el botón navegamos al contact */
            Router.navigate("/contact");
        });

        /* Listener aviso legal */
        document.getElementById("avisoLegal").addEventListener("click",
        function(){
            /* Cuando se aprieta el botón navegamos a aviso legal */
            Router.navigate("/avisolegal");
        });

        /* Listener politica de cookies */
        document.getElementById("temPolCookies").addEventListener("click",
        function(){
            /* Cuando se aprieta el botón navegamos a cookies */
            Router.navigate("/cookies");
        });

        /* Listener tarifas, muestra todas las tarifas */
        document.getElementById("menuTable__tarifesBtn").addEventListener("click",
        function(){
            Router.navigate("/tarifas");
        });

        /* Listener para caalogo */
        document.getElementById("menuTable__catalegBtn").addEventListener("click",
        function(){
            Router.navigate("/cataleg");
        });
        document.getElementById("selectLanguage").addEventListener("change", async function(){
            utils.setCookie("idioma",document.getElementById("selectLanguage").value,365);
            //Router.navigate("/"+await Router.getFragment());
        });
});