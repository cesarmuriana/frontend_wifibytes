
    /**Home controller
     * @class
     */
class HomeControler {

    constructor(tarifas, empresa, textos) {
        tarifas = tarifas.results;

        let activo = true;
        document.getElementById("body").innerHTML = this.renderHome();
        
        /* Pinta las tarifas, si no existe ninguna tarifa no se pintara ninguna (la imagen de fondo
            de las tarifas tampoco aparecerà) */
        if (tarifas.length > 0){
            document.getElementById("tarifas").style.backgroundImage = "url('./images/fondo.png')";
            tarifas.forEach(element => {
                document.getElementById("tarifas").innerHTML += this.renderTarifas(element);
            });
        }

        /**
         * Despues de recoger los textos se pintan en el home dependiendo del idioma que
         * hayamos escogido
         * @param {array} txt 
         */
        textos.forEach(element => {
            /* Si entra activo = false porque si que se muestra el elemento que se quiere */
            if (element.lang === document.getElementById("selectLanguage").value && element.activo){
                document.getElementById("infoHome").innerHTML = this.renderTextos(element);
                activo = false;
            }else{
                /* En caso de que no se cumpla la anterior se mostrara cualquier idioma activo */
                if(activo && element.activo){
                    document.getElementById("infoHome").innerHTML = this.renderTextos(element);
                    activo = false;
                    alert("No esta disponible el idioma seleccionado, se ha seleccionado el primero disponible")
                }else if(!activo && !element.activo){
                    alert("El idioma "+element.lang+" no esta disponible")
                }
            }
        });

        /**
         * Se pasa los datos de la empresa y se filtran los textos a lo que contenga 
         * jumbotron y se hace un map para resolver la url de dichas imagenes
         * y se envian a la template del carrusel
         *
         */
        let imagenes = empresa.textos.filter((value)=>{
            return value.key.match(/jumbotron/);
        }).map((value)=>{
            return value.image;
        });
        document.getElementById("carrusel").innerHTML = this.carruselHTML(imagenes);

        document.querySelector('#toggle-animation').addEventListener('click', function(){
            var el = document.querySelector('.icon-cards__content');

            if (!el.classList.contains("step-animation")){
                el.classList.add("step-animation")
            }else{
                el.classList.toggle('step-animation');
            }
        });
    

        /* Se recogen los datos de la empresa y se envia a la funcion anterior para resolver
        la url de las imagenes */

    }

    /* render  */
    renderHome(){
        return `
        <div id="carrusel">
        </div>
        <div id="tarifas">
        </div>
        <div id="infoHome">
        </div>`;
    }

    renderTextos(value){
        return ` <div id="bloqueIzquierda"> <h1 id="titulo_izquierda">${value.caja_izquierda_titulo}</h1> <div id="div_izquierda"> <p id="text_izquierda">${value.caja_izquierda_texto}</p> </div> </div> <div id="bloqueDerecha"> <h1 id="titulo_derecha">${value.caja_derecha_titulo}</h1> <div id="div_derecha"> <p id="text_derecha">${value.caja_derecha_texto}</p> </div> </div>`
    }

    renderTarifas(value){
        return ` <div id="tarifa"> <div id="tarifa-nombre"> <p id="tarifa-nombre-text"> <img id="tarifa-img" src=${value.logo}> ${value.nombretarifa} </p> </div> <div id="tarifa-precio"> <p id="tarifa-precio-text">${value.precio} €/mes</p> </div>` + this.renderTarifa(value) + `<div id="separador"></div> <div id="tarifa-more"> <button id="button-app-contratar">Contratar</button> <button id="button-app-info">info</button> </div> </div>`;
    }

    renderTarifa(value){
        if (value.subtarifas[0].subtarifa_velocidad_conexion_bajada){ return ` <div id="tarifa-velocidades"> <h3>Fibra</h3> <div id="separador"></div> <p id="tarifa-velocidades-bajada"> <strong>${value.subtarifas[0].subtarifa_velocidad_conexion_bajada} Mb/s</strong> bajada </p> <div id="separador"></div> <p id="tarifa-velocidades-subida"> <strong>${value.subtarifas[0].subtarifa_velocidad_conexion_subida} Mb/s</strong> subida </p> `+ this.renderTv(value) +` </div>` } if (value.subtarifas[0].subtarifa_minutos_gratis){ return `<div id="tarifa-minutos"> <h3>Movil</h3> <div id="separador"></div> <p id="tarifa-minutos-gratis"> <strong>${value.subtarifas[0].subtarifa_minutos_gratis} Min Gratis</strong> </p> <div id="separador"></div> <p id="tarifa-minutos-est"> <strong>${value.subtarifas[0].subtarifa_cent_minuto}€ Cent/Min</strong> </p> </div>` }
    }

    renderTv(value){
        if (value.subtarifas[0].subtarifa_num_canales){ return ` <div id="separador"></div> <p>Canales TV disponibles <strong>${value.subtarifas[0].subtarifa_num_canales}</strong></p> ` }else{ return `<p hidden>Esta tarifa no tiene canales</p>`; }
    }

    carruselHTML(imagenes){

        /* Aci se fa un map per a totes les images que venen, per a fer-li el html a cada imatge sense
        dependre de la cantitat de imatges que venen */
        let a = imagenes.map((value)=>{
            return `<div class="icon-cards__item d-flex align-items-center justify-content-center"><img id="imgcarrusel" src="${value}"/></div>`
        })
    
        return ` <figure class="icon-cards mt-3"> <div class="icon-cards__content step-animation"> ${a} </div> </figure> <div class="checkbox"> <label class="switch"> <input type="checkbox" id="toggle-animation"> <span class="slider round"></span> </label> </div> <div id="soloespacio"></div>`;
    }
}

export default HomeControler;