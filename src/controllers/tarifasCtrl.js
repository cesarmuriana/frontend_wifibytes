
/**
 * El controlador de tarifas, este controlador pinta todas las tarifas activas
 * @class
 */
class TarifasController {

    constructor(value) {
        document.getElementById("body").innerHTML = this.renderHTML();
        document.getElementById("tarifas").style.backgroundImage = "url('./images/fondo.png')";
        value.results.forEach(element => {
            document.getElementById("tarifas").innerHTML += this.renderTarifas(element);    
        });
        
    }
    renderHTML(){
        return `
        <div id="tarifas">
        </div>`
    }
    
    renderTarifas(value){
        return ` <div id="tarifa"> <div id="tarifa-nombre"> <p id="tarifa-nombre-text"> <img id="tarifa-img" src=${value.logo}> ${value.nombretarifa} </p> </div> <div id="tarifa-precio"> <p id="tarifa-precio-text">${value.precio} €/mes</p> </div>` + this.renderTarifa(value) + `<div id="separador"></div> <div id="tarifa-more"> <button id="button-app-contratar">Contratar</button> <button id="button-app-info">info</button> </div> </div>`;
    }

    renderTarifa(value){
        if (value.subtarifas[0].subtarifa_velocidad_conexion_bajada){ return ` <div id="tarifa-velocidades"> <h3>Fibra</h3> <div id="separador"></div> <p id="tarifa-velocidades-bajada"> <strong>${value.subtarifas[0].subtarifa_velocidad_conexion_bajada} Mb/s</strong> bajada </p> <div id="separador"></div> <p id="tarifa-velocidades-subida"> <strong>${value.subtarifas[0].subtarifa_velocidad_conexion_subida} Mb/s</strong> subida </p> `+ this.renderTv(value) +` </div>` } if (value.subtarifas[0].subtarifa_minutos_gratis){ return `<div id="tarifa-minutos"> <h3>Movil</h3> <div id="separador"></div> <p id="tarifa-minutos-gratis"> <strong>${value.subtarifas[0].subtarifa_minutos_gratis} Min Gratis</strong> </p> <div id="separador"></div> <p id="tarifa-minutos-est"> <strong>${value.subtarifas[0].subtarifa_cent_minuto}€ Cent/Min</strong> </p> </div>` }
    }

    renderTv(value){
        if (value.subtarifas[0].subtarifa_num_canales){ return ` <div id="separador"></div> <p>Canales TV disponibles <strong>${value.subtarifas[0].subtarifa_num_canales}</strong></p> ` }else{ return `<p hidden>Esta tarifa no tiene canales</p>`; }
    }
}

export default TarifasController;