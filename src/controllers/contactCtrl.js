/**
 * El controlador de contact, se muestra en el body el contact
 * @class
 */
class ContactControler {

    constructor() {
            document.getElementById("body").innerHTML = this.render();
    }

    /** render  */
    render(){
        return `
            <div id="contactHTML">
                <form id="contact_form" name="contact_form" class="form-contact">
                    <div>
                        <div id="control-group">
                            <label for="sel1">Subject</label>
                                <select class="form-control" id="inputSubject_contact" name="inputSubject_contact" title="Choose subject">
                                <option value="compra">Info relativa a tu compra</option>
                                <option value="evento">Celebra un evento con nosotros</option>
                                <option value="programacion">Contacta con nuestro dpto de programacion</option>
                                <option value="Trabaja">Trabaja con nosotros</option>
                                <option value="proyectos">Deseas proponernos proyectos</option>
                                <option value="sugerencias">Haznos sugerencias</option>
                                <option value="reclamaciones">Atendemos tus reclamaciones</option>
                                <option value="club">Club rural_shop</option>
                                <option value="sociales">Proyectos sociales</option>
                                <option value="festivos">Apertura de festivos</option>
                                <option value="novedades">Te avisamos de nuestras novedades</option>
                                <option value="diferente">Algo diferente</option>
                            </select>
                            <span class="text-danger">Elija una opcion</span>
                            <span class="text-danger">Elija una opcion</span>
                        </div>
                        <div id="email-area">
                            <input type="text" id="contact_email" name="contact_email" placeholder="Introduzca su e-mail"/>
                            <span class="text-danger">The email is required</span>
                            <span class="text-danger">The email is not valid</span>
                        </div>
                        <textarea type="text" id="contact_text" name="contact_text" placeholder="Introduzca el contenido de su consulta"></textarea>
                        <div id="send-area">
                            <button id="enviar">SEND</button>
                        </div>
                    </div>
                </form>
                <div class="gmap_canvas">
                    <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Wifibytes%2C%20Bocairente%2C%20Espa%C3%B1a&amp;t=&amp;z=17&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                    </iframe>
                </div>
            </div>`
    }
}

export default ContactControler;