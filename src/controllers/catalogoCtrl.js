/**Cataleg controller
 * @class
 */
class CatalegController {

    constructor(catalogo) {
        document.getElementById("body").innerHTML = this.render()
        catalogo.results.forEach(element => {
            document.getElementById("catalogo").innerHTML += this.tarifa(element);
        });
    }
    render(){
        return `
            <div id="catalogo">
            </div>
        `;
    }
    tarifa(value){
        return `
            <div id="telefono">
                <img id="img-catalogo" src="${value.imagen}"/>
                <h2 id="content">${value.descripcion}</h2>
                <div id="tarifa-more">
                <h4>PVP ${value.pvp} €/IVA incl</h4>
                    <button id="button-app-contratar">Comprar</button>
                    <button id="button-app-contratar">More info</button>
                </div>
            </div>
        `;
    }
}

export default CatalegController;