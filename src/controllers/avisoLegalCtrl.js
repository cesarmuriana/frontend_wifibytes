import {get} from '../utils';

/**
 * Controlador sobre el aviso legal de la empresa
 * @class
 */
class AvisoLegalController {

    constructor(datosEmpresa) {

        try{
            datosEmpresa = datosEmpresa.textos.filter((value)=>{return value.key.match(/legal/)})
        }catch(err){
            console.log(err)
        }
        document.getElementById("body").innerHTML = this.render(datosEmpresa)
    }

    /** render  */
    render(a){
        return `<div id='avisoLegaltxt'>${a[0].content}</div>`
    }
}

export default AvisoLegalController;