/**
 * El controlador de politica de cookies
 * @class
 */
class CookiesController {

    constructor(datosEmpresa) {

        try{
            datosEmpresa = datosEmpresa.textos.filter((value)=>{return value.key.match(/cookies/)})
        }catch(err){
            console.log(err)
        }
        document.getElementById("body").innerHTML = this.render(datosEmpresa)
    }

    render(a){
        return `
            <div id='cookiesEmpresa'>${a[0].content}</div>`
    }
}

export default CookiesController;