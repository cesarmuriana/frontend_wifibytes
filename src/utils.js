/* Ajax */
import {Settings} from './settings';

/* Objeto que contiene otro objeto con las url necesarias de los endPoints y
las funcions que se usan en toda la aplicacion, se hace de esta forma para solo exportar
utils, asi en el index solo importamos utils*/
let utils = {
    urls:{
        'datosHome': '/home/?format=json',
        'datosEmpresa':'/datos_empresa/?format=json',
        'tarifas':'/tarifa/?destacado=true',
        'allTarifas':'/tarifa/?format=json',
        'articulos':'/articulo/?format=json'
    },
    get(url,callback){
        url = this.urls[url]
        var req = new XMLHttpRequest();
        req.open('GET', Settings.backendURL+url);
        req.onload = function() {
            if (this.readyState === 4 && this.status === 200) {
                callback(req.response);
            }else {
                alert('Request failed.  Returned status of ' + this.status);
            }
        };
        req.send();
    },
    getLocal(datos){
        return new Promise((resolve, reject)=>{
            let value = JSON.parse(localStorage.getItem(datos));
            if (!value){
                this.get(datos,function(response){
                    localStorage.setItem(datos, response);
                    let value = JSON.parse(response);
                    resolve(value);
                });
            }else{
                resolve(value);
            }
        })
    },
    setCookie(name, value, daysDie) {
        var d = new Date();
        d.setTime(d.getTime() + (daysDie*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = name + "=" + value + ";" + expires + ";path=/";
    },
    getCookie(name) {   
        try{
            return document.cookie.match(new RegExp(name+"[\\s]*=[\\s]*([\\w]*)"))[1];
        }catch(e){
            return "";
        }
    }
}

export {utils};