/**
 * Template del footer
 * En empresa vienen los datos de la empresa y se muestran
 * @param {array} empresa 
 */
function footerHTML(empresa){
    return `
    <table id="infEmpresa">
    <tr>
        <p id="textInfo">Información de la empresa:</p>
    </tr>
    <tr>
        <td>
            <p id="name">Nombre de la empresa: ${empresa.name}</p>
            <p id="provincia">Provincia: ${empresa.province}</p>
        </td>
        <td>
            <p id="phone">Numero de contacto: ${empresa.phone}</p>
            <p id="city">Ciudad: ${empresa.city}</p>
        </td>
        <td>
            <p id="zipcode"> Codigo postal: ${empresa.zipcode}</p>
            <p id="address">Calle: ${empresa.address}</p>
        </td>
        <td>
            <div id="redesSociales">
                <a href="${empresa.facebook}"><img id="iconos" src="./images/icono_facebook.png"/></a>
                <a href="${empresa.twitter}"><img id="iconos" src="./images/icono_insta.png" /></a>
            </div>
        </td>
    </tr>
    </table>
    <div id="subfooter">
    <table>
        <tr>
            <td>
                <p id="avisoLegal">Aviso legal</p>
            </td>
            <td>
                <p id="temPolCookies">Politica de cookies</p>
            </td>
        </tr>
    </table>
    </div>

    `;
}
export {footerHTML};