/**
 * Template sobre el aviso de las cookies 
 * @function
 */
function cookiesHTML(){
    return `
    <div>
        <strong>Debido a la normativa europea sobre cookies estamos obligados a mostrarle esta advertencia</strong>
        <p>Para que este sitio funcione adecuadamente, a veces instalamos en los dispositivos de los usuarios pequeños ficheros de datos, conocidos como cookies. La mayoría de los grandes sitios web también lo hacen.</p>
        <button id="borrarCookies">Aceptar</button>
    </div>
    `;
}

export {cookiesHTML};