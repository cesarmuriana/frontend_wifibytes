/**
 * Template del menu
 * Aqui estan los botones del menu y en el array de empresa vienen los datos de la empresa y se muestra el logo de dicha empresa
 * 
 * @param {array} empresa 
 */
function menuHTML(empresa){
    return `
    <div class="menuTable">
      <table>
        <tr>
          <td>
            <a id="menuTable__homeBtn">Home</a>
          </td>
          <td>
            <a id="menuTable__catalegBtn">Cataleg</a>
          </td>
          <td>
            <a id="menuTable__tarifesBtn">Tarifes</a>
          </td>
          <td>
            <img id="imgLogo" src="${empresa.icon_logo}"/>
          </td>
          <td>
            <a id="menuTable__tarifesBtn">Nosotros</a>
          </td>
          <td>
            <a id="menuTable__contactBtn">Contact</a>
          </td>
          <td>
            <a id="menuTable__mycuentaBtn">Mi cuenta</a>
          </td>
        </tr>
      </table>
    </div>`;
}

export {menuHTML};