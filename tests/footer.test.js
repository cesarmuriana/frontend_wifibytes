import {footerHTML} from '../src/templates/footer.js';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML = '<div id="body"></div>';
});

let empresa = {
    "name":"MyBussines",
    "phone":"633799378",
    "address":"Gaspar Blai Arbuixec, 12",
    "city":"Ontinyent",
    "province":"Valencia",
    "zipcode":"46870",
    "facebook":"https://www.facebook.com/cesar.murianagoya",
    "twitter":"https://www.instagram.com/el_cesar_06/",
};

test('Test inyectar template del footer', () => {
    $("#body").innerHTML = footerHTML(empresa);
    expect($('#body').length).toBeGreaterThan(0);
});

test('Test footer inyectar empresa en la template', () => {
    //let aux = footerHTML(empresa).match(/\${[a-zA-Z]*.[a-zA-Z]*}/)
    expect(footerHTML(empresa).match(/\${[a-zA-Z]*.[a-zA-Z]*}/)).toBe(null);
});
