import {cookiesHTML} from '../src/templates/cookiesFloat.js';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="body"></div>';
});

test('Test inyectar template de les cookies', () => {
    $("#body").innerHTML = cookiesHTML();
    expect($('#body').length).toBeGreaterThan(0);
});