import {menuHTML} from '../src/templates/menu.js';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="body"></div>';
});

let empresa = {
    "name":"MyBussines",
    "phone":"633799378",
    "address":"Gaspar Blai Arbuixec, 12",
    "city":"Ontinyent",
    "province":"Valencia",
    "zipcode":"46870",
    "facebook":"https://www.facebook.com/cesar.murianagoya",
    "twitter":"https://www.instagram.com/el_cesar_06/",
};

test('Test inyectar template del menu', () => {
    $("#body").innerHTML = menuHTML(empresa);
    expect($('#body').length).toBeGreaterThan(0);
});

test('Test menu template', () => {
    expect(menuHTML(empresa).match(/\${[a-zA-Z]*.[a-zA-Z]*}/)).toBe(null);
});