#INTRODUCTION

This silly project is only an excuse to explain the basic scaffolding of a SPA web application.
The project is programmed in raw ES6 (ES2015) and transpiled, packaged, uglified and served using as a supporting tools; webpack, babel, webpack-dev-server, jsdoc ...
Could be used as a base to develop any other javascript project during the course

#Key points 
* Unit battery testing with jest
* Hot live reloading with webpack hmr
* PAckaged with webpack
* Transpiled with babel
* Documented with jsdoc

#Webpack

NICE TO READ https://www.valentinog.com/blog/webpack-tutorial/

#Ajax and fetch API 
NICE TO READ: https://dev.to/bjhaid_93/beginners-guide-to-fetching-data-with-ajax-fetch-api--asyncawait-3m1l

#TIPS
To solve some jest testing problems I've been forced to install

npm install babel-core@7.0.0-bridge.0 --save-dev


#Funcionamiento

Cuando el usuario entra en la pagina se descargan todos los datos (textos, tarifas, contenidos textos home, politica de cookies, y aviso legal), siempre y se guardan en el localStorage. Por posibles cambios siempre se descarga la informacion y se guarda, asi solo hay que recargar para ver esos cambios, en los controladores se hace try catc porque añ poner la url directamente el index no carga y para evitar que no haya ninguna informacion, de esta forma al dar error de recoger datos del localStorage lo descarga y guarda.

En cualquier botón para ir a algun sitio ya sea el contact, aviso legal, politica de cookies o el propio home lo que se hace es un listener de dichos botones para que cuando se pinche desde javascript se cambie la url y no recarge la pagina ya que de esta forma cargaria la pagina de nuevo y con ello nuevas peticiones al servidor, y luego se cambia el contenido del body por aquello que queremos.

Cuando se acepta la politica de cookies se guarda en localStorage cookies true, quiriendo decir que ya hemos aceptado, asi a la proxima vez que accedamos a la pagina dicho mensaje ya no aparecerà.

#CAMBIOS

En el js de utils se tiene que cambiar las urls, la aplicacion entera trabaja sobre esas url.